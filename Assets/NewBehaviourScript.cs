﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Video;
using UnityEngine.UI;
using System;

public class NewBehaviourScript : MonoBehaviour {

	public node nod;
	public VideoPlayer video;
	private string json;
	string pathFile;
	public Text mensagem;
	public bool VersionCLientIfTrue;
	private arrayHolder conv;

	[System.Serializable]
	public class DataHolder
	{
		int vframe;
		float x;
		float y;
		float z;
		float w;
	}
	[System.Serializable]
	public class arrayHolder
	{
		DataHolder[] arr;
	}

	// Use this for initialization
	void Start () {
		pathFile = Application.persistentDataPath + Path.DirectorySeparatorChar + "json.txt";
		json = "";
		if(VersionCLientIfTrue)
		{
			StartCoroutine(Gravar());
		}
		else
		{
			read();
		}
	}

	public void read()
	{
		conv = JsonUtility.FromJson<arrayHolder>(ReadString(pathFile));
		//StartCoroutine(Replayar());
	}

	IEnumerator Replayar()
	{
		while(true)
		{
			yield return new WaitForEndOfFrame();
			break;
		}
	}

	// Update is called once per frame
	IEnumerator Gravar () {
		while(true)
		{
			if(video.isPlaying)
			{
				if(nod.eyes[0] != null)
				{
					if(json.Length != 0)
						json += ", ";
					else
						json += "{\"arrayHolder\":[";
					
					Quaternion rotat = nod.eyes[0].transform.rotation;
					json += "{\"vframe\":" + video.frame + 
						", \"x\":" + rotat.x +
						", \"y\":" + rotat.y +
						", \"z\":" + rotat.z +
						", \"w\":" + rotat.w +
						"}";
					
				}
			}
			else if(json.Length > 0)
			{
				json += "]}";
				mensagem.text = pathFile;
				WriteString(pathFile, json);
				break;
			}


			yield return new WaitForEndOfFrame();
		}
	}


	static void WriteString(string path, string conteiner)
	{

		//Write some text to the test.txt file
		FileInfo f = new FileInfo(path);
		f.Delete();
		StreamWriter writer = new StreamWriter(path, true);
		writer.Write(conteiner);
		writer.Close();
	}

	static string ReadString(string path)
	{
		//Read the text from directly from the test.txt file
		StreamReader reader = new StreamReader(path); 
		string text = reader.ReadToEnd();
		reader.Close();
		return text;
	}
}
